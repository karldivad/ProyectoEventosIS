package com.zs.singleton;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

/**
 * Singleton de la calse PersistenceManageFactory
 * @version 1
 *
 */
public final class PersistenceManagerFact {
	private static final PersistenceManagerFactory pmfInstance = JDOHelper
			.getPersistenceManagerFactory("transactions-optional");

	private PersistenceManagerFact() {
	}

	public static PersistenceManagerFactory get() {
		return pmfInstance;
	}
}