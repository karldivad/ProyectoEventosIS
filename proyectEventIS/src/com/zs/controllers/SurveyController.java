package com.zs.controllers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.appengine.api.datastore.KeyFactory;
import com.zs.models.Organizer;
import com.zs.models.Question;
import com.zs.models.Survey;
import com.zs.session.User;
import com.zs.singleton.PersistenceManagerFact;
import com.zs.utils.Pair;
import com.zs.utils.Types;
import com.zs.utils.Utils;

/**
 * Controlador con funciones varias del las Encuestas.
 * @version 1
 *
 */
@Controller
@Scope("request")
@RequestMapping("/survey")
@ComponentScan("com.zs.session")
public class SurveyController {

	@Autowired
	private User user;
	
	/**
	 * Fucnion que trae la pagina con todas las encuestas
	 * @param model
	 * @return Pagina showAllPage
	 */
	@RequestMapping(value = "/showAll", method = RequestMethod.GET)
	public String getAllSurveys(ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		int flag = 0;
		try{
			Query query = pm.newQuery(Survey.class);
			List<Survey> surveyList = (List<Survey>) query.execute();
			model.addAttribute("flag", flag);
			model.addAttribute("surveys", surveyList);
		} finally {
			pm.close();
		}
		return "survey/showAllPage";
	}
	
	/**
	 * Funcion para agregar una pregunta
	 * @param model
	 * @return Pagina addQuestion
	 */
	@RequestMapping(value = "/addQuestion/{id}", method = RequestMethod.GET)
	public String addQuestion(@PathVariable String id, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		int flag = 0;
		
		model.addAttribute("flag", flag);
		model.addAttribute("id", id);
		model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		
		return "survey/addQuestion";
	}
	
	/**
	 * Funcion para agregar una pregunta
	 * @param model
	 * @return Pagina addQuestion
	 */
	@RequestMapping(value = "/addQuestion/{id}", method = RequestMethod.POST)
	public String saveQuestion(@PathVariable String id,HttpServletRequest request,  ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		int flag = 0;
		model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		String question = request.getParameter("question");
		try{
			Survey sv = pm.getObjectById(Survey.class,id);
			Question qst = new Question();
			List<Question> qs = sv.getQuestions();
			qst.setQuestion(question);
			qst.setSurvey(sv);
			qst = pm.makePersistent(qst);
			qs.add(qst);
			sv.setQuestions(qs);
			flag = Types.SUCESS;
			model.addAttribute("flag", flag);
			model.addAttribute("id", id);
		} finally {
			pm.close();
		}
		return "survey/addQuestion";
	}
	
	/**
	 * Funcion para mmostrat la encuesta completa
	 * @param id
	 * @param model
	 * @return Pagina showSurvey
	 */
	@RequestMapping(value = "/showSurvey/{id}", method = RequestMethod.GET)
	public String getSurvey(@PathVariable String id, ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		try{
			Survey sv = pm.getObjectById(Survey.class, id);
			List<Question> qs = sv.getQuestions();
			model.addAttribute("questions", qs);
			model.addAttribute("survey", sv);
		} finally {
			pm.close();
		}
		
		return "survey/showSurvey";
	}
	
	
	/**
	 * Funcion para agregar respuesta a una encuesta
	 * @param id Id de la encuesta
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addAnswer/{id}" , method = RequestMethod.POST)
	public String saveAnswers(@PathVariable String id, HttpServletRequest request, ModelMap model){
		
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		int flag = 0;
		try{
			Survey sv = pm.getObjectById(Survey.class,id);
			List<Question> qsList = sv.getQuestions();
			for(Question qst : qsList){
				//qst = pm.getObjectById(Question.class,KeyFactory.keyToString(qst.getId()));
				String res = request.getParameter(KeyFactory.keyToString(qst.getId()));
				List<String> answers = qst.getAnswers();
				answers.add(res);
				qst.setAnswers(answers);
				qst.setQuestion(qst.getQuestion());
				
			}
			flag = Types.SUCESS;
			Query query = pm.newQuery(Survey.class);
			List<Survey> svList = (List<Survey>) query.execute();
			model.addAttribute("flag", flag);
			model.addAttribute("surveys", svList);
		} finally {
			pm.close();
		}
		return "survey/showAllPage";
	}
	
	/**
	 * Muestra las preguntas de la Encuesta con el id asociado.
	 * @param id Id de la Encuesta
	 * @param model
	 * @return Pagina showQuestionsPage
	 */
	@RequestMapping(value = "/showQuestions/{id}", method = RequestMethod.GET)
	public String showAnswers(@PathVariable String id, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		try{
			Survey sv = pm.getObjectById(Survey.class,id);
			List<Question> qs = sv.getQuestions();
			model.addAttribute("questions", qs);
			model.addAttribute("survey", sv);
		} finally{
			pm.close();
		}
		return "survey/showQuestionsPage";
	}
	
	/**
	 * Retorna la página con las estadisticas de una Encuesta
	 * @param id
	 * @param model
	 * @return Pagina stadisticsPage
	 */
	@RequestMapping(value = "/showStadistics/{id}", method = RequestMethod.GET)
	public String showStadistics(@PathVariable String id, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		List<Pair<String,List<Integer>>> res = new ArrayList<Pair<String,List<Integer>>>();
		try{
			Survey sv = pm.getObjectById(Survey.class,id);
			List<Question> qsList = sv.getQuestions();
			for (Question qst : qsList){
				List<Integer> answers = new ArrayList<Integer>();
				for(int i = 0; i  < 4; i++){
					answers.add(0);
				}
				List<String> anwList = qst.getAnswers();
				for (String anw : anwList){
					int index = Integer.parseInt(anw);
					answers.set(index, answers.get(index) + 1);
				}
				Pair<String,List<Integer>> pr = new Pair<String,List<Integer>>(qst.getQuestion(),answers);
				res.add(pr);
			}
			model.addAttribute("res", res);
			model.addAttribute("survey", sv);
		} finally{
			pm.close();
		}
		return "survey/stadisticsPage";
		
	}
	
	/**
	 * Elimina la pregunta con el id asociado
	 * @param id Id de la Encuesta
	 * @param qId Id de la Pregunta
	 * @param model
	 * @return Redirecciona al controlador showQuestions
	 */
	@RequestMapping(value = "/deleteQuestion/{id}/{qId}", method = RequestMethod.GET)
	public String deleteQuestion(@PathVariable String id, @PathVariable String qId, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		try{
			Question qst = pm.getObjectById(Question.class,qId);
			pm.deletePersistent(qst);
		}finally{
			pm.close();
		}
		return "redirect:/survey/showQuestions/" + id;
	}
	
	/**
	 * Elimina la Encuesta con el id asociado
	 * @param id Id de la Encuesta
	 * @param model
	 * @return Pagina showSurveys
	 */
	@RequestMapping(value = "/delete/{id}" , method = RequestMethod.GET)
	public String showSurveys(@PathVariable String id, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		try{
			Query query = pm.newQuery(Survey.class);
			Survey s = pm.getObjectById(Survey.class,id); 
			pm.deletePersistent(s);
			List<Survey> svList = (List<Survey>) query.execute();
			model.addAttribute("flag", 0);
			model.addAttribute("surveys", svList);
			model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		}finally{
			pm.close();
		}
		return "organizer/showSurveys";
	}
	
	/**
	 * Retorna el formulario para agregar una Encuesta
	 * @param model
	 * @return Pagina addSurveyForm
	 */
	@RequestMapping(value = "/addSurvey" , method = RequestMethod.GET)
	public String getAddSurveyForm(ModelMap model){		
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		model.addAttribute("flag",0);
		return "organizer/addSurveyForm";
	}

	/**
	 * Agrega una encuesta
	 * @param request
	 * @param model
	 * @return Pagina addSurveyForm
	 */
	@RequestMapping(value = "/addSurvey" , method = RequestMethod.POST)
	public String addSurvey(HttpServletRequest request, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}

		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String title = request.getParameter("title");
		String descripcion = request.getParameter("descripcion");
		String iniDay = request.getParameter("iniDay");
		String iniMonth = request.getParameter("iniMonth");
		String iniYear = request.getParameter("iniYear");
		String endDay = request.getParameter("endDay");
		String endMonth = request.getParameter("endMonth");
		String endYear = request.getParameter("endYear");
		model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		int flag = 0;
		try{
			Date ini = Utils.getDate(iniDay, iniMonth, iniYear);
			Date end = Utils.getDate(endDay, endMonth, endYear);
			if (ini.compareTo(end) > 0){
				flag = Types.ADD_SURVEY_DATE_ERROR;
				model.addAttribute("flag", flag);
				return "organizer/addSurveyForm";
			}
			//if(o.getTasks() == null) o.setTasks(new ArrayList<Task>());
			Survey s = new Survey();
			s.setTitle(title);
			s.setDescripcion(descripcion);
			s.setStartDate(ini);
			s.setEndDate(end);
			s = pm.makePersistent(s);
			model.addAttribute("flag", Types.SUCESS);
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			pm.close();
		}
		return "organizer/addSurveyForm";

	}

	/**
	 * Retorna la página con todas las encuestas
	 * @param model
	 * @return Pagina showSurveys
	 */
	@RequestMapping(value = "/showSurveys" , method = RequestMethod.GET)
	public String showSurveys(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		model.addAttribute("type", Types.TypeOrganizerMapping(or.getCategory()));
		try{
			Query query = pm.newQuery(Survey.class);
			List<Survey> s = (List<Survey>) query.execute();
			model.addAttribute("surveys",s);
			model.addAttribute("flag", 0);
			//model.addAttribute("organizer",o);
		}finally{
			pm.close();
		}
		return "organizer/showSurveys";
	}

}
