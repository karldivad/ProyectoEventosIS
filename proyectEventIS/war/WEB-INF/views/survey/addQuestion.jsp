<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Survey" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	int flag = (Integer) request.getAttribute("flag");
	String id = (String) request.getAttribute("id");
	int type = (Integer) request.getAttribute("type");
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Agregar Pregunta</h2>
	   <p> Digite su pregunta para la encuesta.</p>
	  </div>
	</div>
	<div class="container">
	<%
		if (flag != 0){
			if (flag == Types.SUCESS){
	%>
			<div class="alert alert-success">
    		<strong>Añadida!</strong> Su encuesta ha sido añadida.
  			</div>
	<%
			}
		}
	%>	
	<form class="form-horizontal" method="post" action = "/survey/addQuestion/<%=id%>">
		<div class="form-group">
			<label class="control-label col-sm-2" for="question">Pregunta:</label>
			<div class="col-sm-4">
				<input type = "text" required="" class="form-control" name = "question" id = "question" placeholder="Digite su pregunta">
			</div>
			<button type="submit" class="btn btn-default" id = "agregar" name = "agregar">Agregar</button>
			<button class="btn btn-default" type="button" id = "Back"
			 <%
			 	if(type == Types.ORGANIZER_COORDINATOR_TYPE){
			 %>
			 onclick = "location.href='/survey/showSurveys'"
			 <%
			 	}
			 	else if(type == Types.ORGANIZER_BOSS_TYPE){
			 %>
			 onclick = "location.href='/survey/showSurveys'"
			 <%
			 	}
			 %>
			 >Atrás</button>
		</div>
	</form>
	
	
	<footer><p>© 2017 ZealotSoft</p></footer>
	</div>
	</body>
</html>