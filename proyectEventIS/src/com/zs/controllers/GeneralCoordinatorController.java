package com.zs.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.JDOUserException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.Transactional;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.zs.utils.Types;
import com.zs.models.Comite;
import com.zs.models.Organizer;
import com.zs.session.User;
import com.zs.singleton.PersistenceManagerFact;
import com.zs.utils.EmailSendService;
import com.zs.utils.Utils;

/**
 * Controlador del Coordinator con sus funciones basicas.
 * @version 1
 *
 */
@Controller
@Scope("request")
@RequestMapping("/coordinator")
@ComponentScan("com.zs.session")
public class GeneralCoordinatorController {
	
	@Autowired
	private User user;
	
	/**
	 * Retorna la interfaz del Coordinator.
	 * @param model
	 * @return Pagina CoordinatorPage.
	 */
	@RequestMapping(value = "/interface", method = RequestMethod.GET)
	public String getGeneralCoordinatorPage(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		return "coordinator/CoordinatorPage"; 
	}
	
	/**
	 * Retorna la pagina para agregar organizadores.
	 * @param model
	 * @return Pagina addOrganizerForm.
	 */
	@RequestMapping(value = "/addOrganizer" , method = RequestMethod.GET)
	public String getAddOrganizerForm(ModelMap model){
		Organizer or = user.getOrganizer();
		int flag = 0;
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Comite.class);
		List<Comite> comList = new ArrayList<Comite>();
		
		model.addAttribute("flag", flag);
		model.addAttribute("type", Types.ORGANIZER_COORDINATOR_TYPE);
		query.setOrdering("name asc");
		comList.add(Utils.getOrganizerComite());
		try{
			comList.addAll((List<Comite>) query.execute());
			model.addAttribute("comites", comList);
		} finally{
			pm.close();
		}
		return "coordinator/addOrganizerForm";
	}
	
	/**
	 * Añade un Organizador.
	 * @param request
	 * @param model
	 * @return Pagina addOrganizerForm.
	 */
	@RequestMapping(value = "/addOrganizer" , method = RequestMethod.POST)
	public String addOrganizer(HttpServletRequest request, ModelMap model){
		Organizer orUser = user.getOrganizer();
		
		if (!Utils.loginAccess(orUser, Types.ORGANIZER_BOSS_TYPE, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		EmailSendService ess = new EmailSendService();
		String email = request.getParameter("email");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String comite = request.getParameter("comite");
		String category = request.getParameter("category");
		int flag = Types.ADD_ORGANIZER_EMAIL_DUPLICATE_ERROR;
		Query queryA = pm.newQuery(Comite.class);
		Query queryB = pm.newQuery(Organizer.class);
		List<Comite> comites = new ArrayList<Comite>();
		List<Organizer> organizers = new ArrayList<Organizer>();
		
		email = email.toLowerCase();
		comite = comite.toLowerCase();
		model.addAttribute("type", Types.TypeOrganizerMapping(orUser.getCategory()));
		if (Utils.validateEmail(email) == false){
			flag = Types.ADD_ORGANIZER_EMAIL_WRONG_ERROR;
		}
		queryA.setOrdering("name asc");
		comites.add(Utils.getOrganizerComite());
		queryB.setFilter("email == emailParam");
		queryB.declareParameters("String emailParam");
		try{
			comites.addAll((List<Comite>) queryA.execute());
			if (flag != Types.ADD_ORGANIZER_EMAIL_WRONG_ERROR){
				organizers = (List<Organizer>) queryB.execute(email);
				if (organizers.size() == 0){
					Organizer or = new Organizer();
					or.setEmail(email);
					or.setFirstName(firstName);
					or.setLastName(lastName);
					or.setComite(comite);
					or.setCategory(category);
					or = pm.makePersistent(or);
					String msg = Utils.MSG_HEADER + Utils.URL_ROOT 
							+ "/organizer/verifyAccount/" +  KeyFactory.keyToString(or.getId());
					ess.send(msg, email, Utils.VERIFY_ACOOUNT_SUBJECT);
					or.setPassword(Utils.cifrar(KeyFactory.keyToString(or.getId()), Utils.C_PO_C));
					flag = Types.SUCESS;	
				}
			}
		} finally{
			pm.close();
			model.addAttribute("flag", flag);
			if (Utils.compareTypes(orUser.getCategory(), Types.ORGANIZER_BOSS_TYPE)){
				List<Comite> comList = new ArrayList<Comite>();
				Comite com = new Comite();
				
				com.setName(orUser.getComite());
				comList.add(com);
				model.addAttribute("comites", comList);
			} else{
				model.addAttribute("comites", comites);
			}
			
		}
		return "coordinator/addOrganizerForm"; 
	}
	
	/**
	 * Retorna la pagina para añadir un comite.
	 * @param model
	 * @return Pagina addComite.
	 */
	@RequestMapping(value = "/addComite" , method = RequestMethod.GET)
	public String getAddComite(ModelMap model){
		Organizer or = user.getOrganizer();
		int flag = 0;
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		model.addAttribute("flag", flag);
		return "coordinator/addComiteForm";
	}
	
	/**
	 * Añade un nuevo comite.
	 * @param request
	 * @param model
	 * @return Pagina addComitrForm.
	 */
	@RequestMapping(value = "/addComite" , method = RequestMethod.POST)
	public String addComite(HttpServletRequest request, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if(!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String name = request.getParameter("comiteName");
		String des = request.getParameter("descripcion");
		int flag = Types.ADD_COMITE_DUPLICATE_ERROR;
		Query query = pm.newQuery(Comite.class);
		List<Comite> comites = new ArrayList<Comite>();
		
		name = name.toLowerCase();
		query.setFilter("name == nameParam");
		query.declareParameters("String nameParam");
		try{
			comites = (List<Comite>) query.execute(name);
			if (comites.size() == 0){
				Comite com = new Comite();
				
				com.setName(name);
				com.setDescripcion(des);
				pm.makePersistent(com);
				flag = Types.SUCESS;
				return "coordinator/addComiteForm";
			}
		} finally{
			pm.close();
			model.addAttribute("flag", flag);
		}
		return "coordinator/addComiteForm";
		
	}
	
	
	/**
	 * Muestra todos lo Organizadores.
	 * @param model
	 * @return Pagina showAllOrganizers.
	 */
	@RequestMapping(value = "/showAllOrganizers" , method = RequestMethod.GET)
	public String showAllOrganizers(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if(!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Organizer.class);
		List<Organizer> result = new ArrayList<Organizer>();
		
		query.setOrdering("comite asc");
		try{
			result = (List<Organizer>) query.execute();
			model.addAttribute("result", result);
			model.addAttribute("type", Types.ORGANIZER_COORDINATOR_TYPE);
		}finally{
			pm.close();
		}
		return "coordinator/showAllOrganizers";
	}
	
	/**
	 * Muestra todos los Comites.
	 * @param model
	 * @return Pagina showAllComites.
	 */
	@RequestMapping(value = "/showAllComites" , method = RequestMethod.GET)
	public String showAllComites(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if(!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Comite.class);
		List<Comite> comites = new ArrayList<Comite>();
		
		query.setOrdering("name asc");
		comites.add(Utils.getOrganizerComite());
		try{
			comites.addAll((List<Comite>) query.execute());
			model.addAttribute("comites", comites);
		} finally{
			pm.close();
		}
		return "coordinator/showAllComites";
	}
	
	/**
	 * Elimina el Organizador con el id asociado.
	 * @param id Id del Organizer.
	 * @param request
	 * @param model
	 * @return Redireccionamiento a un controlador.
	 */
	@RequestMapping(value = "/deleteOrganizer/{id}" , method = RequestMethod.GET)
	public String deleteOrganizer(@PathVariable String id, HttpServletRequest request, ModelMap model){
		Organizer orUser = user.getOrganizer();
		
		if(!Utils.loginAccess(orUser, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Organizer or = pm.getObjectById(Organizer.class, id);
			pm.deletePersistent(or);
		} finally{
			pm.close();
		}
		if(Utils.compareTypes(orUser.getCategory(), Types.ORGANIZER_COORDINATOR_TYPE)){
			return "redirect:/coordinator/showAllOrganizers";
		}
		return "redirect:/organizer/showWorkers";
	}
	
	/**
	 * Elimina el Comite con el id asociado.
	 * @param id Id del Comite.
	 * @param request
	 * @param model
	 * @return Redirecciona al controlador showAllComites.
	 */
	@RequestMapping(value = "/deleteComite/{id}" , method = RequestMethod.GET)
	public String deleteCOmite(@PathVariable String id, HttpServletRequest request, ModelMap model){
		Organizer orUser = user.getOrganizer();
		
		if(!Utils.loginAccess(orUser, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Organizer.class);
		
		query.setFilter("comite == comiteParam");
		query.declareParameters("String comiteParam");
		try{
			Comite com = pm.getObjectById(Comite.class,id);
			List<Organizer> orList = (List<Organizer>) query.execute(com.getName());
			
			pm.deletePersistentAll(orList);
			pm.deletePersistent(com);
		} finally{
			pm.close();
		}
		return "redirect:/coordinator/showAllComites";
	}
	
	
}
