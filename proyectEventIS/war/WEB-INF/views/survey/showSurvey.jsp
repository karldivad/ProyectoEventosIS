<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Survey" %>
<%@ page import="com.zs.models.Question" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	List<Question> qs = (List<Question>)  request.getAttribute("questions");
	Survey sv = (Survey) request.getAttribute("survey"); 
%>


<html>
	<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2><%=sv.getTitle() %></h2>
	   <p> Por favor, elija las opciones correspondientes a los campos.</p>
	  </div>
	</div>
	<div class="container">
	
	<form class="form-horizontal" method="post" action = "/survey/addAnswer/<%=KeyFactory.keyToString(sv.getId())%>">
	
	
	<%
		for (Question qst : qs){
	%>
		<%=qst.getQuestion()%>
		<fieldset class="form-group" id = <%=KeyFactory.keyToString(qst.getId())%>>
			<label class="form-check-label"><input type = "radio" class="form-check-input" value = "<%=Integer.toString(Types.SURVEY_ANSWER_VERY_GOOD)%>" name = "<%=KeyFactory.keyToString(qst.getId())%>" checked> Muy Bueno </label>
			<label class="form-check-label"><input type = "radio" class="form-check-input" value = "<%=Integer.toString(Types.SURVEY_ANSWER_GOOD)%>" name = "<%=KeyFactory.keyToString(qst.getId())%>"> Bueno </label>
			<label class="form-check-label"><input type = "radio" class="form-check-input" value = "<%=Integer.toString(Types.SURVEY_ANSWER_BAD)%>" name = "<%=KeyFactory.keyToString(qst.getId())%>"> Malo </label>
			<label class="form-check-label"><input type = "radio" class="form-check-input" value = "<%=Integer.toString(Types.SURVEY_ANSWER_VERY_BAD)%>" name = "<%=KeyFactory.keyToString(qst.getId())%>"> Muy Malo </label>	
		</fieldset>
	<%
		}
	%>
	<button class="btn btn-primary" type="submit" id = "Crear">Guardar</button>
	<button class="btn btn-default" type="button" id = "Back" onclick = "location.href='/survey/showAll'">Back</button>
	</form>
	</div>
	<footer><p>© 2017 ZealotSoft</p></footer>
	</body>
</html>