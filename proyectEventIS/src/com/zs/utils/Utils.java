package com.zs.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zs.models.Comite;
import com.zs.models.Organizer;

/**
 * Clase de Utilidades.
 * @version 1
 *
 */
public class Utils {
	
	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
 
	public static final String MSG_HEADER = "EVENTUS 2.0 Lo invita a usted a ser parte de la organización"
			+ " de un gran evento. </br> Haga click en el siguiente enlace para completar su registro: </br>";
	
	public static final String MSG_FORGOT_PASS_HEADER = "Usted ha solicitado un cambio de contraseña </br>"
			+ "Haga click en el siguiente enlace: </br>";
	
	public static final String URL_ROOT = "www.csunsa.com";
	
	public static final String VERIFY_ACOOUNT_SUBJECT = "EVENTUS 2.0 - Confirmar registro";
	
	public static final String FORGOT_PASS_SUBJECT = "EVENTUS 2.0 - Cambio de Contraseña";
	
	/**
	 * Clave para la codificación Cesar para encriptar las password de los Organizadores.
	 */
	public static final int C_PO_C = 171717;
	
	//public static final int C_IDO_C = 131516;
	
	//public static final int C_IDC_C = 161513;
	
		
	public Utils() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Verifica si la fecha del arguneto paso la fecha actual
	 * @param date
	 * @return boolean
	 */
	public static boolean passTime(Date date){
		if (date == null){
			return true;
		}
		Date actualDate = new Date();
		int res = actualDate.compareTo(date);
		return res > 0;
	}
	
	/**
	 * COnvierte una fecha de Strign a formato Date.
	 * @param day
	 * @param month
	 * @param year
	 * @return Date
	 * @throws ParseException 
	 */
	public static Date getDate(String day, String month, String year) throws ParseException{
		try {
			if (year.length() != 4){
				throw new ParseException("error",0);
			}
			if (Integer.parseInt(month) > 31){
				throw new ParseException("error",0);
			}
			if (Integer.parseInt(year) % 4 == 0 && Integer.parseInt(year) % 400 != 0){
				if (month == "2" || month == "02"){
					if (Integer.parseInt(month) > 29){
						throw new ParseException("error",0);
					}
				}
			} else{
				if (month == "2" || month == "02"){
					if (Integer.parseInt(month) > 28){
						throw new ParseException("error",0);
					}
				}
			}
			
			
			String fecha = day + "-" + month + "-" + year;
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
			Date fechaEnviar = null;
			fechaEnviar = formatoDelTexto.parse(fecha);
            return fechaEnviar;
        } catch (ParseException ex) {
            throw ex;
        }
	}
	
	/**
	 * Retorna la fecha en un String
	 * @param date
	 * @param type Formato en que se va a mostrat la fecha 
	 * @return La fecha en tipo String
	 */
	public static String getDate(Date date, int type){
		if (date == null){
			return "00-00-0000";
		}
		SimpleDateFormat formateador = null;
		switch (type){
			 case Types.DATE_NORMAL:
				 formateador = new SimpleDateFormat("dd-MM-yyyy");
				 break;
			 case Types.DATE_DAY:
				 formateador = new SimpleDateFormat("dd");
				 break;
			 case Types.DATE_MONTH:
				 formateador = new SimpleDateFormat("MM");
				 break;
			 case Types.DATE_YEAR:
				 formateador = new SimpleDateFormat("yyyy");
				 break;
		}
        return formateador.format(date);
	}
	
	/**
	 * Cifrado Cesar. Encriptacion.
	 * @param original Frase a encriptar
	 * @param clave Clave cesar.
	 * @return Frase encriptada.
	 */
	public static String cifrar(String original, int clave){
        StringBuilder cifrado = new StringBuilder(original.length());
        int valorASCII = 0;
        
        for (int i = 0; i < original.length(); i++){
            valorASCII = (int) (original.charAt(i));
            valorASCII = valorASCII + clave % 255;
            cifrado.append((char)(valorASCII));
        }
        return cifrado.toString();
    }

	/**
	 * Cifrado Cesar. Desencriptacion.
	 * @param original Frase a desencriptar.
	 * @param clave Clave cesar.
	 * @return Frase desencriptada.
	 */
    public static String descifrar(String original, int clave){
        StringBuilder descifrado = new StringBuilder(original.length());
        int ASCIIcifrado = 0; 
        int n = 0;
        
        for (int i = 0; i < original.length(); i++){
            ASCIIcifrado = (int) (original.charAt(i));
            ASCIIcifrado = ASCIIcifrado - clave % 255;
            descifrado.append((char) (ASCIIcifrado));
        }
        return descifrado.toString();
    }

    /**
     * Valida un email.
     * @param email Email a validar.
     * @return True, si el email es correcto, False en el otro caso.
     */
	public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
	
	/**
	 * Cambia el primer caracter de la cadena a mayúscula.
	 * @param cadena
	 * @return Cadena modificada.
	 */
	public static String firstToUpperCase(String cadena){
		if (cadena.length() == 0) {
			return cadena;
		}
		if (cadena.length() == 1) {
			return cadena.toUpperCase();
		}
		String resultado = cadena.substring(0, 1).toUpperCase() + cadena.substring(1);
		return resultado;
	}
	
	/**
	 * 
	 * @return Comite Organizador.
	 */
	public static Comite getOrganizerComite(){
		Comite comite = new Comite();
		
		comite.setName("Comite Organizador");
		comite.setDescripcion("Comité encargado de la organización general y la planificación del evento. Este comité no se puede borrar");
		return comite;
	}
	
	/**
	 * Compara un Tipo de Organizador String con uno int.
	 * @param typeA Primer tipo String.
	 * @param typeB Segundo tipo int.
	 * @return True si son iguales, False en el caso contrario.
	 */
	public static boolean compareTypes(String typeA, int typeB){
		int typeATemp = Types.TypeOrganizerMapping(typeA);
		return typeATemp == typeB;
	}
	
	/**
	 * Verifica el acceso al login
	 * @param or Organizador en la session.
	 * @param types Typos con los que se quiere verificar.
	 * @return True si es que el organizador tiene acceso, False en el caso contrario.
	 */
	public static boolean loginAccess(Organizer or, int ... types){
		if(or == null){
			return false;
		}		
		String typeOr = or.getCategory();
		
		for (int type : types){
			if (compareTypes(typeOr, type)){
				return true;
			}
		}
		return false;
	}
	
	
	
}
