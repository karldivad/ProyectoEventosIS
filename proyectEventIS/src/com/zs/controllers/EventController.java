package com.zs.controllers;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zs.singleton.PersistenceManagerFact;
import com.zs.utils.Types;
import com.zs.utils.Utils;
import com.zs.models.Comite;
import com.zs.models.DeadLine;
import com.zs.models.Event;
import com.zs.models.Organizer;
import com.zs.session.User;

/**
 * Controlador para la entidad Event.
 * @version 1
 *
 */
@Controller
@Scope("request")
@RequestMapping("/event")
@ComponentScan("com.zs.session")
public class EventController {
	
	@Autowired
	private User user;
	
	/**
	 * Retorna la pagina con las caracteristicas del evento
	 * @param model
	 * @return Pagina features
	 */
	@RequestMapping(value = "/getFeatures", method = RequestMethod.GET)
	public String getFeaturesPage(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		int flag = 0;
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Query query = pm.newQuery(Event.class);
			List<Event> events = (List<Event>) query.execute();
			Event event = events.get(0);
			model.addAttribute("flag", flag);
			model.addAttribute("event", event);
		} finally {
			pm.close();
		}
		return "event/features";
	}
	
	/**
	 * Graba las caracteristicas del evento.
	 * @param model
	 * @param request
	 * @return Pagina features
	 */
	@RequestMapping(value = "/saveFeatures", method = RequestMethod.POST)
	public String saveFeatures(ModelMap model, HttpServletRequest request){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		int flag = 0;
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String name = request.getParameter("name");
		String description = request.getParameter("descripcion");
		String institution = request.getParameter("institution");
		String country = request.getParameter("country");
		String phone = request.getParameter("phone");
		int assistants = Integer.parseInt(request.getParameter("assistants"));
		int speakers = Integer.parseInt(request.getParameter("speakers"));
		
		try{
			Query query = pm.newQuery(Event.class);
			List<Event> events = (List<Event>) query.execute();
			Event event = pm.getObjectById(Event.class,events.get(0).getId());
			event.setName(name);
			event.setDescription(description);
			event.setInstitution(institution);
			event.setPhone(phone);
			event.setCountry(country);
			event.setAssistants(assistants);
			event.setSpeakers(speakers);
			flag = Types.SUCESS;
			model.addAttribute("flag", flag);
			model.addAttribute("event", event);
		} finally {
			pm.close();
		}
		return "event/features";
	}
	
	/**
	 * Retorna la pagina de DeadLines
	 * @param model
	 * @return Pagina deadLinePage
	 */
	
	@RequestMapping(value = "/getDeadlineForm", method = RequestMethod.GET)
	public String getDeadlineForm(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Query query = pm.newQuery(DeadLine.class);
			List<DeadLine> dlList = (List<DeadLine>) query.execute();
			model.addAttribute("deadlines",dlList);
			
		}finally{
			pm.close();
		}
		
		return "event/deadLinePage";
	}
	
	/**
	 * Retorna la pagina para modificar deadlines
	 * @param id
	 * @param model
	 * @return Pagina addDeadLineForm
	 */
	@RequestMapping(value = "/modifyDeadLine/{id}", method = RequestMethod.GET)
	public String getmodifyAddLine(@PathVariable String id, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		int flag = Types.DEADLINE_MODIFY;
		try{
			DeadLine dl = pm.getObjectById(DeadLine.class,id);
			model.addAttribute("deadline", dl);
			model.addAttribute("flag", flag);
			
		} finally{
			pm.close();
		}
		return "event/addDeadLineForm";
	}
	
	/**
	 * Modifica el DeadLine
	 * @param id Id del deadline
	 * @param model
	 * @return Pagina addDeadLineForm
	 */
	@RequestMapping(value = "/modifyDeadLine/{id}", method = RequestMethod.POST)
	public String modifyAddLine(@PathVariable String id, ModelMap model, HttpServletRequest request){
		Organizer or = user.getOrganizer();

		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		String descripcion = request.getParameter("descripcion");
		String iniDay = request.getParameter("iniDay");
		String iniMonth = request.getParameter("iniMonth");
		String iniYear = request.getParameter("iniYear");
		String endDay = request.getParameter("endDay");
		String endMonth = request.getParameter("endMonth");
		String endYear = request.getParameter("endYear");
		int flag = 0;	
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
	
		try{
			DeadLine dl = pm.getObjectById(DeadLine.class,id);
			Date ini = Utils.getDate(iniDay, iniMonth, iniYear);
			Date end = Utils.getDate(endDay, endMonth, endYear);
			if (ini.compareTo(end) > 0){
				flag = Types.DEADLINE_MODIFY_DATE_ERROR;
				model.addAttribute("flag", flag);
				model.addAttribute("deadline",dl);
				return "event/addDeadLineForm";
			}
			dl.setDescription(descripcion);
			dl.setDateIni(ini);
			dl.setDateEnd(end);
			model.addAttribute("deadline", null);
			flag = Types.DEADLINE_MODIFY_SUCESS;
			model.addAttribute("flag", flag);
		} catch (ParseException e) {
			DeadLine dl = pm.getObjectById(DeadLine.class,id);
			flag = Types.DEADLINE_MODIFY_DATE_PARSE_ERROR;
			model.addAttribute("deadline", dl);
			model.addAttribute("flag", flag);
			return "event/addDeadLineForm";
		} finally{
			pm.close();
		}
		
		return "event/addDeadLineForm";
	}
	
	/**
	 * Retorna la pagina para añadir un nuevo deadline
	 * @param model
	 * @return Pagina addDeadLineForm
	 */
	@RequestMapping(value = "/addDeadLine", method = RequestMethod.GET)
	public String getAddDeadLinesForm(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		int flag = 0;
		model.addAttribute("flag", flag);
		model.addAttribute("deadline", null);
		return "event/addDeadLineForm";
	}
	
	
	/**
	 * Ingresa el deadline a la basae de datos
	 * @param model
	 * @param request
	 * @return Pagina addDeadLineForm
	 */
	@RequestMapping(value = "/addDeadLine", method = RequestMethod.POST)
	public String addDeadLine(ModelMap model, HttpServletRequest request){
		Organizer or = user.getOrganizer();

		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		String descripcion = request.getParameter("descripcion");
		String iniDay = request.getParameter("iniDay");
		String iniMonth = request.getParameter("iniMonth");
		String iniYear = request.getParameter("iniYear");
		String endDay = request.getParameter("endDay");
		String endMonth = request.getParameter("endMonth");
		String endYear = request.getParameter("endYear");
		int flag = 0;
		
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Date ini = Utils.getDate(iniDay, iniMonth, iniYear);
			Date end = Utils.getDate(endDay, endMonth, endYear);
			if (ini.compareTo(end) > 0){
				flag = Types.DEADLINE_DATE_ERROR;
				model.addAttribute("flag", flag);
				model.addAttribute("deadline", null);
				return "event/addDeadLineForm";
			}
			DeadLine dead = new DeadLine();
			Query query = pm.newQuery(Event.class);
			List<Event> eventList = (List<Event>) query.execute();
			Event event = pm.getObjectById(Event.class,eventList.get(0).getId());
			List<DeadLine> dlList = event.getDeadLines();
			dead.setDateIni(ini);
			dead.setDateEnd(end);
			dead.setDescription(descripcion);
			dead.setEvent(event);
			dead = pm.makePersistent(dead);
			dlList.add(dead);
			event.setDeadLines(dlList);
			flag = Types.SUCESS;
			model.addAttribute("flag", flag);
			model.addAttribute("deadline", null);
		} catch (ParseException e) {
			flag = Types.DEADLINE_DATE_PARSE_ERROR;
			model.addAttribute("flag", flag);
			model.addAttribute("deadline", null);
			return "event/addDeadLineForm";
		}finally{
			pm.close();
		}
		return "event/addDeadLineForm";
	}
	
	/**
	 * Elimina un DeadLine con su respectivo id
	 * @param id Id de DeadLine
	 * @param model
	 * @return Pagina deadlinePage
	 */
	@RequestMapping(value = "/deleteDeadLine/{id}", method = RequestMethod.GET)
	public String deleteDeadLine(@PathVariable String id, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		try{
			DeadLine dead = pm.getObjectById(DeadLine.class,id);
			pm.deletePersistent(dead);
			Query query = pm.newQuery(DeadLine.class);
			List<DeadLine> dlList = (List<DeadLine>) query.execute();
			model.addAttribute("deadlines",dlList);
		} finally{
			pm.close();
		}
		return "event/deadLinePage";
	}
}
