package com.zs.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zs.models.Comite;
import com.zs.models.Event;
import com.zs.models.Item;
import com.zs.models.ItemLog;
import com.zs.models.Organizer;
import com.zs.session.User;
import com.zs.singleton.PersistenceManagerFact;
import com.zs.utils.Types;
import com.zs.utils.Utils;

@Controller
@Scope("request")
@RequestMapping("/inventory")
@ComponentScan("com.zs.session")
public class InventoryController {
	
	@Autowired
	private User user;
	
	
	/**
	 * Retorna la interfaz del inventario
	 * @param model
	 * @return Pagina inventoryPage
	 */
	@RequestMapping(value = "/interface", method = RequestMethod.GET)
	public String getInventoryInterface(ModelMap model){
		if(user.getLoginFlag() != 1){
			if(user.getLoginFlag() != 0 && user.getLoginFlag() != 1) user.setLoginFlag(0);
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Item.class);
		List<Item> items = new ArrayList<Item>();
		int flag = 0;
		
		model.addAttribute("type",Types.TypeOrganizerMapping(user.getOrganizer().getCategory()));
		model.addAttribute("flag", flag);
		query.setOrdering("name ASC");
		try{
			items = (List<Item>) query.execute();
			model.addAttribute("items",items);
		}finally{
			pm.close();
			
		}
		return "inventory/inventoryPage";
	}
	
	/**
	 * Retorna el formulario para añadir un Item
	 * @param model
	 * @return Pagina addItemForm
	 */
	@RequestMapping(value = "/addItem", method = RequestMethod.GET)
	public String getAddItemForm(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		
		int flag = 0;
		
		model.addAttribute("flag", flag); 
		return "inventory/addItemForm";
	}
	
	/**
	 * Añade el Item respectivo
	 * @param model
	 * @param request
	 * @return Pagina addItemForm
	 */
	@RequestMapping(value = "/addItem", method = RequestMethod.POST)
	public String addItem(ModelMap model, HttpServletRequest request){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		
		String name = request.getParameter("name");
		int flag = 0;
		
		if (name.length() == 0){
			flag = Types.ADD_ITEM_EMPTY_NAME_ERROR;
			model.addAttribute("flag", flag);
			return "inventory/addItemForm";
		}
		
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Item.class);
		
		query.setFilter("name == nameParam");
		query.declareParameters("String nameParam");
		try{
			List<Item> itemList = new ArrayList<Item>();
			
			itemList = (List<Item>) query.execute(name);
			if (itemList.size() == 0){
				Item itm = new Item();
				itm.setItemCounter(0);
				itm.setName(name);
				pm.makePersistent(itm);
				ItemLog iLog = new ItemLog();
				iLog.setItemName(itm.getName());
				iLog.setType(Types.ITEMLOG_CREATE_TYPE);
				iLog.setDate(new Date());
				pm.makePersistent(iLog);
				flag = Types.SUCESS;
				model.addAttribute("flag", flag);
			} else{
				flag = Types.ADD_ITEM_DUPLICATE_NAME_ERROR;
				model.addAttribute("flag", flag);
				return "inventory/addItemForm";
			}
		}finally{
			pm.close();
			
		}
		return "inventory/addItemForm";
	}
	
	
	/**
	 * Guarda los ItemCounters de todos los Items
	 * @param model
	 * @param request
	 * @return Pagina inventoryPage
	 */
	@RequestMapping(value = "/saveCounters", method = RequestMethod.POST)
	public String saveInventoryCounters(ModelMap model, HttpServletRequest request){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Item.class);
		List<Item> items = new ArrayList<Item>();
		List<Item> changedItems = new ArrayList<Item>();
		int flag = 0;
		
		model.addAttribute("type",Types.TypeOrganizerMapping(user.getOrganizer().getCategory()));
		try{
			items = (List<Item>) query.execute();
			for (Item itm : items){
				int tempCounter = Integer.parseInt(request.getParameter(itm.getName()));
				if (tempCounter < 0){
					itm.setItemCounter(0);
				} else if(itm.getItemCounter() != tempCounter){
					ItemLog iLog = new ItemLog();
					iLog.setItemName(itm.getName());
					if(itm.getItemCounter() < tempCounter){
						iLog.setType(Types.ITEMLOG_UPDATE_POSITIVE_TYPE);
						iLog.setExtraVal(tempCounter - itm.getItemCounter());
					} else {
						iLog.setType(Types.ITEMLOG_UPDATE_NEGATIVE_TYPE);
						iLog.setExtraVal(itm.getItemCounter() - tempCounter);
					}
					iLog.setDate(new Date());
					pm.makePersistent(iLog);
					itm.setItemCounter(tempCounter);
				} else {
					itm.setItemCounter(tempCounter);
				}
				changedItems.add(itm);
			}
			flag = Types.SUCESS;
			model.addAttribute("items", changedItems);
			model.addAttribute("flag", flag);
		} finally{
			pm.close();
		}
		return "inventory/inventoryPage";
	}

	/**
	 * Elimina un Item
	 * @param id Id del Item
	 * @param model
	 * @return Redirecciona al controlador /inventory/interface
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteItem(@PathVariable String id, ModelMap  model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Item itm = pm.getObjectById(Item.class,id);
			ItemLog iLog = new ItemLog();
			iLog.setItemName(itm.getName());
			iLog.setType(Types.ITEMLOG_DELETE_TYPE);
			iLog.setDate(new Date());
			pm.makePersistent(iLog);
			pm.deletePersistent(itm);
		} finally {
			pm.close();
		}
		return "redirect:/inventory/interface";
	}
	
	/**
	 * Retorna la interfaz del historial
	 * @param model
	 * @return Pagina historialInterface
	 */
	@RequestMapping(value = "/historial/interface", method = RequestMethod.GET)
	public String getHistorialPage(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(ItemLog.class);
		List<ItemLog> logsList = new ArrayList<ItemLog>();
		
		try{
			logsList = (List<ItemLog>) query.execute();
			model.addAttribute("logs", logsList);
		} finally{
			pm.close();
		}
		
		return "inventory/historialInterface";
	}
}
