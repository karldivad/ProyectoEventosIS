<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Survey" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	int flag = (Integer) request.getAttribute("flag");
	List<Survey> svList = (List<Survey>) request.getAttribute("surveys");
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Mis Encuestas</h2>
	   <p> Por favor, elija alguna encuesta para completar.</p>
	  </div>
	</div>
	<div class="container">
	<%
		if (flag != 0){
			if (flag == Types.SUCESS){
	%>
		<div class="alert alert-success">
    		<strong>Guardado!</strong> Sus respuestas han sido guardadas correctamente.
  			</div>
	<%
			}
		}
	%>	
		<div class="table-responsive">
		 <table class="table table-striped">
		 	<tr>
				<th>Encuesta</th>
				<th>Fecha de inicio</th>
				<th>Fecha Final</th>
				<th>Estado</th>
			</tr>

	<%
		for (Survey sv : svList){
	%>
		<tr>
		<td>
	<%
		if (!Utils.passTime(sv.getEndDate()) && Utils.passTime(sv.getStartDate())){
	%>
		<a href ="/survey/showSurvey/<%=KeyFactory.keyToString(sv.getId())%>"><%=sv.getTitle()%></a>
	<%
		} else{
	%>
		<%=sv.getTitle()%>
	<%
		}
	%>
		</td>
		<td><%=Utils.getDate(sv.getStartDate(), Types.DATE_NORMAL)%></td>
		<td><%=Utils.getDate(sv.getEndDate(), Types.DATE_NORMAL)%></td>
		<td>
	<%
		if(Utils.passTime(sv.getEndDate())){
	%>
		Terminada
	<%
		} else if(!Utils.passTime(sv.getStartDate())){
	%>
		Esperando fecha de Inicio
	<%
		} else {
	%>
		Activa
	<%
		}
	%>
		</td>
		</tr>
	<%
		}
	%>
	</table>
	<button class="btn btn-default" type="button" id = "Back" onclick = "location.href='/'">Back</button>
	</div>
	<div id="chartContainer" style="height: 300px; width: 100%;"></div>
	<footer><p>© 2017 ZealotSoft</p></footer>
	</div>
	</body>
</html>