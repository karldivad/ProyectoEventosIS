<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Item" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	int flag = (Integer) request.getAttribute("flag");
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="#">Inicio</a></li>
            <li><a onclick = "location.href='/inventory/interface'">Mi Panel</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>              
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h1>Inventario</h1>
	   <p> Agrege un nuevo elemento a su inventario.</p>
	  </div>
	 </div>
	 <div class="container">
    
	<%
		if (flag != 0){
			if (flag == Types.SUCESS){
	%>
		<div class="alert alert-success">
    		<strong>Guardado!</strong> Su elemento se ha agregado exitosamente!
  			</div>
	<%
			} else if (flag == Types.ADD_ITEM_EMPTY_NAME_ERROR){
	%>
		<div class="alert alert-danger">
    			<strong>Oh my!</strong> El elemento no puedo tener nombre vacio o nulo.
  			</div>
	<%
			} else if (flag == Types.ADD_ITEM_DUPLICATE_NAME_ERROR){
	%>
		<div class="alert alert-danger">
    			<strong>Oh my!</strong> El elemento ya existe, pruebe con uno diferente.
  			</div>
	<%
			}
		}
	%>
		<form class="form-horizontal" method="post" action = "/inventory/addItem">
			<div class="form-group">		
				<label class="control-label col-sm-2" for="email">Nombre:</label>
				<div class="col-sm-4"> 
					<input type = "text" required="" class="form-control" name = "name" id = "name" placeholder="Ingrese un nombre">
				</div>
			</div>
			<div class="col-sm-offset-2 col-sm-10">
			<button class="btn btn-default" type="submit" id = "Crear">Crear</button>
			<button class="btn btn-default" type="button"  id = "Back" onclick = "location.href='/inventory/interface'">Back</button>
			</div>
		</form>
		</div>
		<footer><p>© 2017 ZealotSoft</p></footer>
		</body>
</html>