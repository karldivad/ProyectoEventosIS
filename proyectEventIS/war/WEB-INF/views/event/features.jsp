<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Event" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	int flag = (Integer) request.getAttribute("flag");
	Event event = (Event) request.getAttribute("event");
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li><a onclick = "location.href='/coordinator/interface'">Mi Panel</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>              
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Características</h2>
	   <p> En está sección puede modificar o agregar las características del evento.</p>
	  </div>
	</div>
	<%
		if(flag != 0){
			if(flag == Types.SUCESS){
	%>
		<div class="alert alert-success">
    		<strong>Guardado!</strong> Características guardadas correctamente.
  		</div>
		
	<%
			}
		}
	%>
	
	<form class="form-horizontal" method = "post" action = "/event/saveFeatures">
		<div class="form-group"><label class="control-label col-sm-2" for="name">Nombre del Evento: </label><div class="col-sm-4"><input type = "text" required="" name = "name" id = "name" value = "<%=event.getName()%>"></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="desc">Descripción del Evento: </label><div class="col-sm-4"><input type = "text" required="" name = "descripcion" id = "descripcion" value = "<%=event.getDescription()%>"></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="inst">Institución: </label><div class="col-sm-4"><input type = "text" required="" name = "institution" id = "institution" value = "<%=event.getInstitution()%>"></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="country">País: </label><div class="col-sm-4"><input type = "text" required="" name = "country" id = "country" value = "<%=event.getCountry()%>"></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="phone">Teléfono: </label><div class="col-sm-4"><input type = "number" min="1" required="" name = "phone" id = "phone" value = "<%=event.getPhone()%>"></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="mail">Correo Electrónico: </label><div class="col-sm-4"><label><%=event.getEmail()%> </label></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="category">Categoría:</label> <div class="col-sm-4"><label><%=event.getCategory()%></label> </div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="type">Tipo: </label><div class="col-sm-4"><label><%=event.getType() %> </label></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="date">Fecha: </label><div class="col-sm-4"><label><%=Utils.getDate(event.getDate(), Types.DATE_NORMAL)%></label> </div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="totalA">Asistentes: </label><div class="col-sm-4"><input type = "number" min="1" required="" name = "assistants" id = "assistants" value = "<%=event.getAssistants()%>"></div></div>
		<div class="form-group"><label class="control-label col-sm-2" for="totalP">Ponentes: </label><div class="col-sm-4"><input type = "number"  min="1" required="" name = "speakers" id = "speakers" value = "<%=event.getSpeakers()%>"></div></div>
		<div class="form-group"><div class="col-sm-offset-2 col-sm-2"><button class="btn btn-default" type="submit" name = "Save" id = "Save" value = "Guardar">Guardar</button><button class="btn btn-default" type="button" id = "Back" onclick = "location.href='/coordinator/interface'">Regresar</button></div></div> 
	</form>
	<div class="col-sm-offset-1 col-sm-0">
	<footer><p>© 2017 ZealotSoft</p></footer>
	</div>
</html>