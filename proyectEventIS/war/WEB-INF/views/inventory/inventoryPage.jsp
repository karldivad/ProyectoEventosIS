<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Item" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	int flag = (Integer) request.getAttribute("flag");
	int type = (Integer) request.getAttribute("type");
	List<Item> items = (List<Item>) request.getAttribute("items");
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="#">Inicio</a></li>
            <li><a <%
					if (type == Types.ORGANIZER_COORDINATOR_TYPE){
				%>
					onclick = "location.href='/coordinator/interface'"	
				<%
					} else if (type == Types.ORGANIZER_BOSS_TYPE){
				%>			
					onclick = "location.href='/organizer/getBossInterface'"
				<%
					} else if (type == Types.ORGANIZER_WORKER_TYPE){
				%>			
					onclick = "location.href='/organizer/getWorkerInterface'"
				<%
					}
				%>>Mi Panel</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>              
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Inventario</h2>
	   <p> En esta sección podra visualizar el inventario.</p>
	  </div>
	 </div>
	 <div class="container">
	<%
		if (flag != 0){
			if (flag == Types.SUCESS){
	%>

		<div class="alert alert-success">
    		<strong>Guardado!</strong> Su inventario se ha guardado exitosamente.
  			</div>

	<%
			}
		}
	%>

		<form class="form-horizontal" method="post" action = "/inventory/saveCounters">
		<table class="table table-striped">
			<tr>
				<th>Nombre</th>
				<th>Stock</th>
			</tr>

	<%
		for (Item itm : items){
	%>
	<tr>
	<td><%=itm.getName()%></td>
	<td><input class="form-control" required="" min="0" type = "number" name = "<%=itm.getName()%>" id = "<%=itm.getName()%>" value ="<%=itm.getItemCounter()%>"  
	   			<%
	   				if (type != Types.ORGANIZER_BOSS_TYPE && type != Types.ORGANIZER_COORDINATOR_TYPE){
	   			%>
	   				disabled
	   			<%
	   				}
	   			%>
	   			><td>
	   			<%
	   				if (type == Types.ORGANIZER_BOSS_TYPE || type == Types.ORGANIZER_COORDINATOR_TYPE){
	   			%>
	   				<a href ="/inventory/delete/<%=KeyFactory.keyToString(itm.getId())%>">Eliminar</a>
	   			<%
	   				}
	   			%>
	   			</br>
	</tr>
	<%
		}
	%>
	</table>
	<%
		if (type == Types.ORGANIZER_BOSS_TYPE || type == Types.ORGANIZER_COORDINATOR_TYPE){
	%>	
		<button class="btn btn-default" type="button"  id = "addItem" onclick = "location.href='/inventory/addItem'">Añadir Item</button>
		<button class="btn btn-default" type="submit" id = "Crear">Guardar</button>
		<button class="btn btn-default" type="button" id = "Crear" onclick = "location.href='/inventory/historial/interface'">Ver Historial</button>
	<%
		}
	%>
		<button class="btn btn-default" type="button"  id = "Back"
				<%
					if (type == Types.ORGANIZER_COORDINATOR_TYPE){
				%>
					onclick = "location.href='/coordinator/interface'"	
				<%
					} else if (type == Types.ORGANIZER_BOSS_TYPE){
				%>			
					onclick = "location.href='/organizer/getBossInterface'"
				<%
					} else if (type == Types.ORGANIZER_WORKER_TYPE){
				%>			
					onclick = "location.href='/organizer/getWorkerInterface'"
				<%
					}
				%>
				>Back</button>
	</form>
	</br>
	<footer><p>© 2017 ZealotSoft</p></footer>
	</div>
	</body>
</html>