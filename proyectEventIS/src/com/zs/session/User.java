package com.zs.session;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.ScopedProxyMode;

import com.zs.models.Organizer;

/**
 * 
 * Encapsula un usuario logeado.
 * @version 1
 */
@Component
@Scope("session")
public class User implements Serializable {
		
	private static final long serialVersionUID = 1L;

	/**
	 * Organizador logeado.
	 */
	private Organizer organizer;
	
	/**
	 * Estado del login.
	 */
	private int loginFlag;

	public User(){
		organizer = null;
		loginFlag = 0;
	}
	
	public Organizer getOrganizer() {
		return organizer;
	}

	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	public int getLoginFlag() {
		return loginFlag;
	}

	public void setLoginFlag(int loginFlag) {
		this.loginFlag = loginFlag;
	}
	
	public void resetLogin(){
		if (loginFlag != 0 && loginFlag != 1) {
			loginFlag = 0;
		}
	}
	
}
