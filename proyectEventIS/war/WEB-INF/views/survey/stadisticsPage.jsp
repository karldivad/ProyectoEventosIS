<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.Survey" %>
<%@ page import="com.zs.models.Question" %>
<%@ page import="com.zs.utils.Pair"%>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	List<Pair<String,List<Integer>>> res = (List<Pair<String,List<Integer>>>) request.getAttribute("res");
	Survey sv = (Survey) request.getAttribute("survey");
%>



<html>
	<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="http://canvasjs.com/assets/script/canvasjs.min.js""></script>
  	
  	
  	
  	<script type="text/javascript">
		window.onload = function () {
		<%
			for (Pair<String,List<Integer>> pr : res){
		%>
		var chart = new CanvasJS.Chart("<%=pr.getFirst()%>",
		{
		title:{
			text: "<%=pr.getFirst()%>"
		},
                animationEnabled: true,
		legend:{
			verticalAlign: "bottom",
			horizontalAlign: "center"
		},
		data: [
		{        
			indexLabelFontSize: 20,
			indexLabelFontFamily: "Monospace",       
			indexLabelFontColor: "darkgrey", 
			indexLabelLineColor: "darkgrey",        
			indexLabelPlacement: "outside",
			type: "pie",       
			showInLegend: true,
			toolTipContent: "{y} - <strong>#percent%</strong>",
			dataPoints: [
				{  y: <%=pr.getSecond().get(Types.SURVEY_ANSWER_VERY_GOOD)%>, legendText:"MB", indexLabel: "Muy Bueno" },
				{  y: <%=pr.getSecond().get(Types.SURVEY_ANSWER_GOOD)%>, legendText:"B", indexLabel: "Bueno" },
				{  y: <%=pr.getSecond().get(Types.SURVEY_ANSWER_BAD)%>, legendText: "M" , indexLabel: "Malo"},
				{  y: <%=pr.getSecond().get(Types.SURVEY_ANSWER_VERY_BAD)%>, legendText:"MM", indexLabel: "Muy Malo" }
				]
			}
			]
		});
		chart.render();
		<%
			}
		%>
		
		}
	</script>
  	
  	
  	
  	
  	
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2><%=sv.getTitle() %></h2>
	   <p> Estadisticas</p>
	  </div>
	</div>
	<div class="container">
	<%
		for (Pair<String,List<Integer>> pr : res){
	%>
			<div id="<%=pr.getFirst()%>" style="height: 300px; width: 100%;"></div></br>
	<%
		}
	%>
	</div>
	<footer><p>© 2017 ZealotSoft</p></footer>
	</body>
</html>