<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.zs.models.Organizer" %>
<%@ page import="com.zs.models.Comite" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import= "com.google.appengine.api.datastore.KeyFactory"%>
<%@ page import="com.zs.utils.Types" %>

<% 
	int flag = (Integer) request.getAttribute("flag");
	Organizer o = (Organizer) request.getAttribute("organizer");
	Comite c = (Comite) request.getAttribute("comite");
%>

<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li class="active"><a href="#">Mi Panel</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>              
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Verificar Cuenta</h2>
	   <p> Por favor, verifique si sus datos son correctos.</p>
	  </div>
	</div>
	<div class="container">
		<%
			if(flag == Types.VERYFYPASS_USER_ERROR){
		%>
			
			<div class="alert alert-danger">
    			<strong>Oh my!</strong> Usted ha sido eliminado de la organización. Comuníquese con su superior para resolver el inconveniente.
  			</div>
			
			<button class="btn btn-default" type="button" id = "Init" onclick = "location.href='/'">Regresar</button>
			
		<%
			}
			else if(flag == Types.VERYFYPASS_DOUBLE_VERYFY_ERROR){
		%>
			<div class="alert alert-info">
    			<strong>Oh my!</strong> Usted ya ha verificado su cuenta.
  			</div>
			
			
			<button class="btn btn-default" type="button" id = "Init" onclick = "location.href='/'">Regresar</button>
					
					
		<%
			}
			else if(flag == Types.SUCESS || flag == Types.VERYFYPASS_PASS_ERROR){
				if(flag == Types.VERYFYPASS_PASS_ERROR){
				
		%>
			<div class="alert alert-warning">
    			<strong>Oh my!</strong> Las contraseñas no coinciden.
  			</div>
			
				
		<%
				}
		%>
				
		<form class="form-horizontal" method = "post" action = "<%=KeyFactory.keyToString(o.getId())%>">
			<div class="table-responsive">
		 		<table class="table table-striped">
				<tr>
					<th>Yo soy</th>
					<th>Mi Comíte</th>
					<th>Descripción</th>
				</tr>
				<tr>
					<td><%=Utils.firstToUpperCase(c.getName())%></td>
					<td><%=o.getCategory()%></td>
					<td><%=c.getDescripcion()%></td>
				</tr>
				</table>
			</div>
			
			<div class="alert alert-info">
    			<strong>Verifique sus datos e ingrese su contraseña:</strong>
  			</div>
		
			
			<div class="form-group"><label class="control-label col-sm-2" for="name">Nombre:</label> <div class="col-sm-4"><input type = "text" required="" name = "firstName" id = "firstName" value = "<%=o.getFirstName()%>"></div></div>
			<div class="form-group"><label class="control-label col-sm-2" for="surname">Apellido:</label> <div class="col-sm-4"><input type = "text" required="" name = "lastName" id = "lastName" value = "<%=o.getLastName()%>"></div></div>
			<div class="form-group"><label class="control-label col-sm-2" for="pass">Contraseña:</label> <div class="col-sm-4"><input type = "password" required="" name = "pass" id = "pass"> </div></div>
			<div class="form-group"><label class="control-label col-sm-2" for="repass">Repetir Contraseña:</label> <div class="col-sm-4"><input type = "password" required="" name = "passRep" id = "passRep"> </div></div>
			
			<div class="form-group">		
			<div class="col-sm-offset-2 col-sm-4">
				<button class="btn btn-default" type="submit" name = "Save" id = "Save" value = "Guardar">Enviar</button>
			</div>
			</div>
			
		</form>
		
		<%
			}
			else if(flag == Types.VERYFYPASS_SUCESS){
		%>
			<div class="alert alert-success">
    			<strong>Oh my!</strong> Sus datos han ido guardados correctamente.
  			</div>
			
			
			<button class="btn btn-default" type="button" id = "Init" onclick = "location.href='/login/'">Continuar</button>
			
		<%
			}
		%>
		</div>
		<footer><p>© 2017 ZealotSoft</p></footer>
	</body>
</html>