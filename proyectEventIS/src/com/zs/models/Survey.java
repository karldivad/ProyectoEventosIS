package com.zs.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Survey implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String title;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private Date startDate;
	
	@Persistent
	private Date endDate;

	@Persistent(mappedBy = "survey")
	@Element(dependent = "true")
	private List<Question> questions = new ArrayList<Question>();
	
	public Key getId(){
		return this.id;
	}
	
	public void setId(Key id){
		this.id = id;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getdescripcion(){
		return this.descripcion;
	}
	

	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	


	
	public Survey(){
		super();
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}
