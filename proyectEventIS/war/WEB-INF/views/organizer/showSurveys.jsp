<%@page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.zs.models.Survey" %>
<%@ page import="com.zs.models.Organizer" %>
<%@ page import="java.util.List" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="com.zs.utils.Utils" %>
<%
	int flag = (Integer) request.getAttribute("flag");
	List<Survey> surveys = (List<Survey>) request.getAttribute("surveys");
	int type = (Integer) request.getAttribute("type");
%>

<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
            <li class="dropdown">
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Encuestas</h2>
	  </div>
	</div>
	<div class="container">
		<h2>Encuestas</h2>
		<div class="table-responsive">
		<table class="table table-striped">
		<tr>
		<th>Título</th>
		<th>Fecha de inicio</th>
		<th>Fecha final</th>
		<th>descripcion</th>
		</tr>

		<%
			if(surveys != null){
				for(Survey t : surveys){
		%>
				<tr>
				<td><%=t.getTitle()%></td>
				<td><%=Utils.getDate(t.getStartDate(), Types.DATE_NORMAL)%></td>
				<td><%=Utils.getDate(t.getEndDate(), Types.DATE_NORMAL)%></td>
                <td><%=t.getdescripcion()%></td>
				<td>
		
					<a href="/survey/delete/<%=KeyFactory.keyToString(t.getId())%>">Eliminar</a>
					<a href="/survey/showQuestions/<%=KeyFactory.keyToString(t.getId())%>">Mostrar Preguntas</a>
					<a href="/survey/addQuestion/<%=KeyFactory.keyToString(t.getId())%>">Agregar Pregunta</a>
					<a href="/survey/showStadistics/<%=KeyFactory.keyToString(t.getId())%>">Ver estadisticas</a>
		
				</td>
		<%
				}
			}
		%>
		</table>
		</div>
		<button class="btn btn-default" type="button" id = "Back"
			 <%
			 	if(type == Types.ORGANIZER_COORDINATOR_TYPE){
			 %>
			 onclick = "location.href='/coordinator/interface'"
			 <%
			 	}
			 	else if(type == Types.ORGANIZER_BOSS_TYPE){
			 %>
			 onclick = "location.href='/organizer/getBossInterface'"
			 <%
			 	}
			 %>
			 >Atrás</button>
		<hr></hr>
      	<footer><p>© 2017 ZealotSoft</p></footer>
		</div>
	</body>
</html>
