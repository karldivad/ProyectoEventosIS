package com.zs.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.JDOFatalUserException;
import javax.jdo.JDOUserException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.zs.models.Comite;
import com.zs.models.Organizer;
import com.zs.models.Survey;
import com.zs.models.Task;
import com.zs.session.User;
import com.zs.singleton.PersistenceManagerFact;
import com.zs.utils.Utils;
import com.zs.utils.Types;

/**
 * Controlador con funciones varias del Organizer
 * @version 1
 *
 */
@Controller
@Scope("request")
@RequestMapping("/organizer")
@ComponentScan("com.zs.session")
public class OrganizerController {

	@Autowired
	private User user;
	
	/**
	 * Retorna la pagina para verificar la cuenta.
	 * @param id Id del Organizador
	 * @param model
	 * @return Pagina verifyAccountForm.
	 */
	@RequestMapping(value = "/verifyAccount/{id}" , method = RequestMethod.GET)
	public String getVerifyAccountForm(@PathVariable String id, ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		int flag = 0;
		
		try{
			Organizer or = pm.getObjectById(Organizer.class,id);
			String pass = Utils.descifrar(or.getPassword(), Utils.C_PO_C);
			
			if (pass.equals(id)){
				
				/*
				 * Si la id es igual a la pass, significa que nunca ha verificado su cuenta. 
				 */
				
				Query query = pm.newQuery(Comite.class);
				query.setFilter("name == comiteName");
				query.declareParameters("String comiteName");
				List<Comite> res = (List<Comite>) query.execute(or.getComite());
				
				
				model.addAttribute("organizer", or);
				if (res.size() == 0){
					model.addAttribute("comite", Utils.getOrganizerComite());
				} else{
					model.addAttribute("comite", res.get(0));
				}
				flag = Types.SUCESS;
			} else {
				flag = Types.VERYFYPASS_DOUBLE_VERYFY_ERROR;
			}
		} catch (JDOUserException e){
			flag = Types.VERYFYPASS_USER_ERROR;
		} catch (JDOFatalUserException e){
			flag = Types.VERYFYPASS_USER_ERROR;
		} finally {
			pm.close();
			model.addAttribute("flag", flag);
		}
		return "organizer/verifyAccountForm";
	}

	/**
	 * Metodo que actualiza los datos del nuevo organizador.
	 * @param id
	 * @param request
	 * @param model
	 * @return Pagina verifyAccountForm.
	 */
	@RequestMapping(value = "/verifyAccount/{id}" , method = RequestMethod.POST)
	public String verifyAccountForm(@PathVariable String id, HttpServletRequest request, ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String pass = request.getParameter("pass");
		String passRep = request.getParameter("passRep");
		int flag = 0;
		
		try{
			Organizer or = pm.getObjectById(Organizer.class,id);
			
			if (pass.equals(passRep) == false){
				Query query = pm.newQuery(Comite.class);
				query.setFilter("name == comiteName");
				query.declareParameters("String comiteName");
				
				List<Comite> res = (List<Comite>) query.execute(or.getComite());
				
				
				model.addAttribute("organizer", or);
				if(res.size() == 0){
					model.addAttribute("comite", Utils.getOrganizerComite());
				} else{
					model.addAttribute("comite", res.get(0));
				}
				flag = Types.VERYFYPASS_PASS_ERROR;
			} else{
				or.setFirstName(firstName);
				or.setLastName(lastName);
				or.setPassword(Utils.cifrar(pass, Utils.C_PO_C));
				flag = Types.VERYFYPASS_SUCESS;
				user.setLoginFlag(0);
				user.setOrganizer(null);
			}
		}
		catch(JDOUserException e){
			flag = Types.VERYFYPASS_USER_ERROR;
		}
		finally{
			pm.close();
			model.addAttribute("flag", flag);
		}
		return "organizer/verifyAccountForm";
	}
	
	/**
	 * Retorna la interfaz del jefe.
	 * @param request
	 * @param model
	 * @return Pagina bossInterface.
	 */
	@RequestMapping(value = "/getBossInterface" , method = RequestMethod.GET)
	public String getBossInterface(HttpServletRequest request, ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		model.addAttribute("organizer", or);
		return "organizer/bossInterface";
	}
	
	/**
	 * Retorna la interfaz del trabajador.
	 * @param model
	 * @return Pagina workerInterface.
	 */
	@RequestMapping(value = "/getWorkerInterface", method = RequestMethod.GET)
	public String getWorkerInterface(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_WORKER_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		model.addAttribute("organizer", or);
		return "organizer/workerInterface";
	}
	
	/**
	 * Retorna la interfaz para añadir un Trabajador.
	 * @param model
	 * @return Pagina addOrganizerForm.
	 */
	@RequestMapping(value = "/addWorker", method = RequestMethod.GET)
	public String getAddWorkerForm(ModelMap model){
		Organizer or = user.getOrganizer();
		List<Comite> comList = new ArrayList<Comite>();
		Comite com = new Comite();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		model.addAttribute("type", Types.ORGANIZER_BOSS_TYPE);
		com.setName(or.getComite());
		comList.add(com);
		model.addAttribute("comites", comList);
		model.addAttribute("flag", 0);
		return "coordinator/addOrganizerForm";
	}

	
	/**
	 * Retorna la pagina para agregar un Task a un Organizer.
	 * @param id Id del Organizer.
	 * @param model
	 * @return Pagina addTaskForm.
	 */
	@RequestMapping(value = "/addTask/{id}" , method = RequestMethod.GET)
	public String getAddTaskForm(@PathVariable String id, ModelMap model){
		Organizer orUser = user.getOrganizer();
		
		if (!Utils.loginAccess(orUser, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		model.addAttribute("id", id);
		model.addAttribute("type", Types.TypeOrganizerMapping(orUser.getCategory()));
		model.addAttribute("flag", 0);
		try{
			Organizer or = pm.getObjectById(Organizer.class, id);
			model.addAttribute("organizer", or);
		} finally{
			pm.close();
		}
		return "organizer/addTaskForm";
	}
	
	/**
	 * Agrega un Task al Organizador con el id asociado.
	 * @param id Id del Organizer
	 * @param request
	 * @param model
	 * @return Pagina addTaskForm.
	 */
	@RequestMapping(value = "/addTask/{id}" , method = RequestMethod.POST)
	public String addTask(@PathVariable String id, HttpServletRequest request, ModelMap model){
		Organizer orUser = user.getOrganizer();
		
		if(!Utils.loginAccess(orUser, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String title = request.getParameter("title");
		String priority = request.getParameter("priority");
		String descripcion = request.getParameter("descripcion");
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		
		model.addAttribute("id", id);
		model.addAttribute("type", Types.TypeOrganizerMapping(orUser.getCategory()));
		try{
			Organizer or = pm.getObjectById(Organizer.class,id);
		
			if (title.length() == 0){
				model.addAttribute("organizer", or);
				model.addAttribute("flag", Types.ADD_TASK_TITLE_ERROR);
			} else{
				Task task = new Task();
				List<Task> tsList = or.getTasks();
				Date dateEnd = Utils.getDate(day, month, year);
				Date dateIni = new Date(); 
				task.setTitle(title);
				task.setPriority(priority);
				task.setDescripcion(descripcion);
				task.setDone(0);
				task.setOrganizer(or);
				task.setDateEnd(dateEnd);
				task.setDateIni(dateIni);
				task = pm.makePersistent(task);
				tsList.add(task);
				or.setTasks(tsList);
				model.addAttribute("organizer", or);
				model.addAttribute("flag", Types.SUCESS);
			}
		} catch (ParseException e) {
			Organizer or = pm.getObjectById(Organizer.class,id);
			model.addAttribute("organizer", or);
			model.addAttribute("flag", Types.ADD_TASK_DATE_ERROR);
			return "organizer/addTaskForm";
		}finally{
			pm.close();
		}
		return "organizer/addTaskForm";

	}
	
	/**
	 * Retorna la pagina que muestra todos los trabajadores.
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/showWorkers", method = RequestMethod.GET)
	public String showWorkers(ModelMap model){
		Organizer or = user.getOrganizer();
		
		if (!Utils.loginAccess(or, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		Query query = pm.newQuery(Organizer.class);
		
		query.setFilter("category == 'Trabajador' && comite == comiteParam");
		query.declareParameters("String comiteParam");
		try{
			List<Organizer> orList = (List<Organizer>) query.execute(or.getComite());
			model.addAttribute("result", orList);
			model.addAttribute("type", Types.ORGANIZER_BOSS_TYPE);
		}finally{
			pm.close();
		}
		return "coordinator/showAllOrganizers";
	}

	
	/**
	 * Muestra los Tasks del Organizador con el id asociado.
	 * @param id Id del Organizer
	 * @param del Variable que especifica si el boton de eliminar va a aparecer o no
	 * @param model
	 * @return Pagina showTasks
	 */
	@RequestMapping(value = "/showTasks/{id}/{del}" , method = RequestMethod.GET)
	public String showTasks(@PathVariable String id, @PathVariable int del, ModelMap model){
		if (user.getLoginFlag() != 1){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		model.addAttribute("type", Types.TypeOrganizerMapping(user.getOrganizer().getCategory()));
		model.addAttribute("id", id);
		model.addAttribute("flag", del);
		
		try{
			Organizer or = pm.getObjectById(Organizer.class, id);
			List<Task> tsList = or.getTasks();
			
			model.addAttribute("tasks", tsList);
			model.addAttribute("organizer", or);
		} finally{
			pm.close();
		}
		return "organizer/showTasks";
	}
	
	/**
	 * Elimina la Task con el tId asociado del Organizer con el id asociado.
	 * @param id Id del Organizer.
	 * @param tId Id del Task.
	 * @param del Variable que especifica si el boton de eliminar va a aparecer o no.
	 * @param model
	 * @return Redireccion al controlador showTasks.
	 */
	@RequestMapping(value = "/deleteTask/{id}/{tId}/{del}" , method = RequestMethod.GET)
	public String deleteTask(@PathVariable String id, @PathVariable String tId, @PathVariable String del, ModelMap model){
		Organizer orUser = user.getOrganizer();
		
		if (!Utils.loginAccess(orUser, Types.ORGANIZER_COORDINATOR_TYPE, Types.ORGANIZER_BOSS_TYPE)){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Task task = pm.getObjectById(Task.class, tId);
			pm.deletePersistent(task);
		} finally{
			pm.close();
		}
		return "redirect:/organizer/showTasks/" + id + "/" + del;
	}
	
	/**
	 * Cambia el estado de la Task con el tId asociado del Organizer con el id asociado.
	 * @param id Id del Organizer.
	 * @param tId Id del Task.
	 * @param tDone Estado por el que se va a cambiar el Task.
	 * @param del Variable que especifica si el boton de eliminar va a aparecer o no.
	 * @param model
	 * @return Redireccion al controlador showTasks.
	 */
	@RequestMapping(value = "/setDoneTask/{id}/{tId}/{tDone}/{del}" , method = RequestMethod.GET)
	public String setDoneTask(@PathVariable String id, @PathVariable String tId, @PathVariable int tDone, @PathVariable String del,ModelMap model){
		if(user.getLoginFlag() != 1){
			user.resetLogin();
			return "redirect:/login/";
		}
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		
		try{
			Task task = pm.getObjectById(Task.class, tId);
			task.setDone(tDone);
		} finally{
			pm.close();
		}
		return "redirect:/organizer/showTasks/" + id + "/" + del;
	}
	
}
