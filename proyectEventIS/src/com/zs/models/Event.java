package com.zs.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Entidad Evento
 * @verison 1
 *
 */
@PersistenceCapable
public class Event {
	
	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
    private String name;
	
	@Persistent
    private String description;
	
	@Persistent
    private String institution;
	
	@Persistent
    private String country;

	@Persistent
	private String phone;
	
	@Persistent
	private String email;
	
	@Persistent
	private String category;
	
	@Persistent
	private String type;
	
	@Persistent
    private Date date;
	
	@Persistent
	private int assistants;
	
	@Persistent
	private int speakers;

	@Persistent(mappedBy = "event")
	@Element(dependent = "true")
	private List<DeadLine> deadLines = new ArrayList<DeadLine>();
	
	public List<DeadLine> getDeadLines() {
		return deadLines;
	}

	public void setDeadLines(List<DeadLine> deadLines) {
		this.deadLines = deadLines;
	}

	public Key getId() {
		return id;
	}

	public void setId(Key id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Event() {
		super();
	}

	public int getAssistants() {
		return assistants;
	}

	public void setAssistants(int assistants) {
		this.assistants = assistants;
	}

	public int getSpeakers() {
		return speakers;
	}

	public void setSpeakers(int speakers) {
		this.speakers = speakers;
	}
	
	
}
