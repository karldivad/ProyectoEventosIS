package com.zs.utils;

/**
 * Tipos y errores varios.
 * @version 1
 *
 */
public class Types {

	
	public static final int COMITE_TYPE = 0;
	public static final int ORGANIZER_COORDINATOR_TYPE = 1;
	public static final int ORGANIZER_BOSS_TYPE = 2;
	public static final int ORGANIZER_WORKER_TYPE = 3;
	
	public static final int SUCESS = 1;
	
	public static final int USER_UNLOGGED = 0; 
	public static final int USER_LOGGED = 1;
	
	public static final int LOGIN_USER_ERROR = -1;
	public static final int LOGIN_PASS_ERROR = -2;
	
	public static final int FORGOTPASS_USER_ERROR = -1;
	
	public static final int CHANGEPASS_REP_ERROR = -1;
	
	public static final int VERYFYPASS_SUCESS = 2;
	public static final int VERYFYPASS_USER_ERROR = -1;
	public static final int VERYFYPASS_DOUBLE_VERYFY_ERROR = -2;
	public static final int VERYFYPASS_PASS_ERROR = -3;
	
	public static final int ADD_TASK_TITLE_ERROR = -1;
	public static final int ADD_TASK_DATE_ERROR = -2;
	
	public static final int ADD_ORGANIZER_EMAIL_DUPLICATE_ERROR = -1;
	public static final int ADD_ORGANIZER_EMAIL_WRONG_ERROR = -2;
	
	public static final int ADD_COMITE_DUPLICATE_ERROR = -1;
	
	public static final int TASK_DONE = 1;
	public static final int TASK_NOT_DONE = 0;	
	
	public static final int ADD_ITEM_EMPTY_NAME_ERROR = -1;
	public static final int ADD_ITEM_DUPLICATE_NAME_ERROR = -2;
	
	public static final int ITEMLOG_CREATE_TYPE = 0;
	public static final int ITEMLOG_UPDATE_POSITIVE_TYPE = 1;
	public static final int ITEMLOG_UPDATE_NEGATIVE_TYPE = 2;
	public static final int ITEMLOG_DELETE_TYPE = 3;
	
	public static final int DEADLINE_MODIFY = 2;
	public static final int DEADLINE_MODIFY_SUCESS = 3;
	public static final int DEADLINE_DATE_ERROR = -1;
	public static final int DEADLINE_MODIFY_DATE_ERROR = -2;
	public static final int DEADLINE_DATE_PARSE_ERROR = -3;
	public static final int DEADLINE_MODIFY_DATE_PARSE_ERROR = -4;
	
	public static final int DATE_NORMAL = 0;
	public static final int DATE_DAY = 1;
	public static final int DATE_MONTH = 2;
	public static final int DATE_YEAR = 3;
	
	public static final int SURVEY_ANSWER_VERY_BAD = 0;
	public static final int SURVEY_ANSWER_BAD = 1;
	public static final int SURVEY_ANSWER_GOOD  = 2;
	public static final int SURVEY_ANSWER_VERY_GOOD = 3;
	
	public static final int ADD_SURVEY_DATE_ERROR = -1;
	
	/**
	 * Convierte el tipo de Organizador string a int para un mejor manejo en el codigo.
	 * @param type El tipo en string
	 * @return int Tipo de organizador
	 */
	public static int TypeOrganizerMapping(String type){
		if (type.equals("Jefe")){
			return ORGANIZER_BOSS_TYPE;
		} else if (type.equals("Trabajador")){
			return ORGANIZER_WORKER_TYPE;
		} else if (type.equals("Coordinador General")){
			return ORGANIZER_COORDINATOR_TYPE;
		}
		return -1;
	}
	
	/**
	 * Convierte el tipo de Organizador int a string para un mejor manejo en el codigo.
	 * @param type
	 * @return String Tipo de Organizador
	 */
	public static String TypeOrganizerMapping(int type){
		if (type == ORGANIZER_BOSS_TYPE){
			return "Jefe";
		} else if (type == ORGANIZER_COORDINATOR_TYPE){
			return "Coordinador General";
		} else if (type == ORGANIZER_WORKER_TYPE){
			return "Trabajador";
		}
		return null;
	}
	
}
