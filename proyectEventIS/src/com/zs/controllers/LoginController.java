package com.zs.controllers;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.server.spi.config.ApiCacheControl.Type;
import com.google.appengine.api.datastore.KeyFactory;
import com.zs.models.Organizer;
import com.zs.session.User;
import com.zs.singleton.PersistenceManagerFact;
import com.zs.utils.EmailSendService;
import com.zs.utils.Types;
import com.zs.utils.Utils;


/**
 * Controlador con las funciones basicas de un Login.
 * @version 1
 */
@Controller
@Scope("request")
@RequestMapping("/login")
@ComponentScan("com.zs.session")
public class LoginController {
	
	/**
	 * Usuario actual de la sesion.
	 */
	@Autowired
	private User user;
	
	/** 
	 * Metodo para entrar a la pagina principal del login
	 * @param request
	 * @param model
	 * @return pagina principal del login.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getLoginPage(HttpServletRequest request, ModelMap model){
		int flag = user.getLoginFlag();
		
		if (flag == Types.USER_LOGGED){
			return "redirect:/";	
		}
		model.addAttribute("flag", flag);
		return "login/loginPage";
	}
	
	/**
	 * Metodo para desloguearse.
	 * @param request
	 * @param model
	 * @return Redirecciona al controlador Raiz.
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, ModelMap model){
		user.setLoginFlag(Types.USER_UNLOGGED);
		user.setOrganizer(null);
		return "redirect:/";
	}
	
	
	/**
	 * Verifica los datos del login y redirecciona al controlador respectivo.
	 * @param request
	 * @param model
	 * @return Redirecciona al controlador respectivo.
	 */
	@RequestMapping(value = "/getLogin" , method = RequestMethod.POST)
	public String getLogin(HttpServletRequest request, ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		int flag = 0;
		Query query = pm.newQuery(Organizer.class);
		
		email = email.toLowerCase();
		query.setFilter("email == emailParam");
		query.declareParameters("String emailParam");
		try{
			List<Organizer> orList = (List<Organizer>) query.execute(email);
			Organizer or = null;
			int orType = 0;
			
			if (orList.size() == 0){
				flag = Types.LOGIN_USER_ERROR;
				user.setLoginFlag(flag);
				return "redirect:/login/";
			}
			or = orList.get(0);
			if (pass.equals(Utils.descifrar(or.getPassword(), Utils.C_PO_C)) == false){
				flag = Types.LOGIN_PASS_ERROR;
				user.setLoginFlag(flag);
				return "redirect:/login/";
			}
			flag = Types.SUCESS;
			user.setOrganizer(or);
			user.setLoginFlag(flag);
			orType = Types.TypeOrganizerMapping(or.getCategory());
			if (orType == Types.ORGANIZER_BOSS_TYPE){
				return "redirect:/organizer/getBossInterface";
			} else if (orType == Types.ORGANIZER_WORKER_TYPE){
				return "redirect:/organizer/getWorkerInterface";
			} else if (orType == Types.ORGANIZER_COORDINATOR_TYPE){
				return "redirect:/coordinator/interface";
			}
		}finally{
			pm.close();
		}
		return null;
	}
	
	/**
	 * Metodo para entrar a la pagina para recuperar la contraseña.
	 * @param model
	 * @return Pagina forgotPassForm.
	 */
	@RequestMapping(value = "/forgotPass" , method = RequestMethod.GET)
	public String getForgotForm(ModelMap model){
		model.addAttribute("flag", 0);
		return "login/forgotPassForm";
	}
	
	/**
	 * Metodo para verificar el email, y enviar un correo de recuperacion si es correcto.
	 * @param request
	 * @param model
	 * @return Pagina forgotPassForm.
	 */
	@RequestMapping(value = "/forgotPass" , method = RequestMethod.POST)
	public String sendForgotEmail(HttpServletRequest request, ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		EmailSendService em = new EmailSendService();
		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		Query query = pm.newQuery(Organizer.class);
		int flag = 0;
		
		email = email.toLowerCase();
		query.setFilter("email == emailParam");
		query.declareParameters("String emailParam");
		try{
			List<Organizer> orList = (List<Organizer>) query.execute(email);
			
			if (orList.size() == 0){
				flag = Types.FORGOTPASS_USER_ERROR;
			} else{
				String msg = Utils.MSG_FORGOT_PASS_HEADER + Utils.URL_ROOT
						+ "/login/forgotPass/changePass/" 
						+ KeyFactory.keyToString(orList.get(0).getId());
				
				em.send(msg, email, Utils.FORGOT_PASS_SUBJECT);
				flag = Types.SUCESS;
			}
		} finally{
			model.addAttribute("flag", flag);
			pm.close();
		}
		return "login/forgotPassForm";
	}
	
	/**
	 * Metodo que retorna la pagina para cambiar la contraseña.
	 * @param id Id del organizador pasado por URL.
	 * @param model
	 * @return Pagina changeForgotPassForm.
	 */
	@RequestMapping(value = "/forgotPass/changePass/{id}", method = RequestMethod.GET)
	public String getChangeForgotPassForm(@PathVariable String id, ModelMap model){
		model.addAttribute("id", id);
		model.addAttribute("flag", 0);
		return "login/changeForgotPassForm";
	}
	
	/**
	 * Metodo que cambia la contraseña del organizador con el id asociado.
	 * @param id Id del organizador
	 * @param request
	 * @param model
	 * @return Pagina changeForgotPassForm.
	 */
	@RequestMapping(value = "/forgotPass/changePass/{id}" , method = RequestMethod.POST)
	public String changeForgotPass(@PathVariable String id, HttpServletRequest request, ModelMap model){
		PersistenceManager pm = PersistenceManagerFact.get().getPersistenceManager();
		String pass = request.getParameter("pass");
		String passRep = request.getParameter("passRep");
		int flag = 0;
		
		model.addAttribute("id",id);
		if (pass.equals(passRep) == false){
			flag = Types.CHANGEPASS_REP_ERROR;
			model.addAttribute("flag", flag);
			return "login/changeForgotPassForm";
		}
		try{
			Organizer or = pm.getObjectById(Organizer.class,id);
			
			or.setPassword(Utils.cifrar(pass, Utils.C_PO_C));
			flag = Types.SUCESS;
		} finally{
			pm.close();
			model.addAttribute("flag", flag);
		}
		return "login/changeForgotPassForm";
	}
	
}
