package com.zs.models;

import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * 
 * Entidad ItemLog para formar el historial del inventario
 * @version 1
 */
@PersistenceCapable
public class ItemLog {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private int type;
	
	@Persistent
	private String ItemName;
	
	@Persistent
	private Date date;

	@Persistent
	private int extraVal;
	
	public Key getId() {
		return id;
	}

	public void setId(Key id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getItemName() {
		return ItemName;
	}

	public void setItemName(String itemName) {
		ItemName = itemName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getExtraVal() {
		return extraVal;
	}

	public void setExtraVal(int extraVal) {
		this.extraVal = extraVal;
	}
	
	
}

