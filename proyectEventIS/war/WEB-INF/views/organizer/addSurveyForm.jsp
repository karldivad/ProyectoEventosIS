<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.zs.utils.Types" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.models.Organizer" %>
<%
	int type = (Integer) request.getAttribute("type");
	int flag = (Integer) request.getAttribute("flag");
%>

<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/">Inicio</a></li>
            <li class="active"><a href="#">Mi Panel</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Agregar Encuesta </h2>
	  </div>
	</div>
	<div class="container">
	<%
		if(flag != 0){
			if(flag == 1){
	%>
			<div class="alert alert-success">
    		<strong>Correcto!</strong> Encuesta ingresada correctamente.
  			</div>
  	<%
			} else if(flag == Types.ADD_SURVEY_DATE_ERROR){				
  	%>	
			<div class="alert alert-danger">
    			<strong>Oh my!</strong> La fecha final debe ser despues de la fecha de inicio.
  			</div>
	<%
			}
		}
	%>


		<form class="form-horizontal" method = "post" action = "/survey/addSurvey">
		<div class="form-group">
			<label class="control-label col-sm-2" >Título:</label>
			<div class="col-sm-4">
				<input type = "text" class="form-control" name = "title" id = "title" placeholder="Ingrese el nombre de la encuesta">
			</div>
		</div>
			<div class="form-group"><label class="control-label col-sm-2" for="date"> Día/Mes/Año de Inicio:</label> <div class="col-sm-2"><input type = "text" pattern="0[1-9]|1[0-9]|2[0-9]|3[01]" required="" id = "iniDay" name = "iniDay"></div>
			 <div class="col-sm-2"><input type = "text" pattern="0[1-9]|1[012]" required="" id = "iniMonth" name = "iniMonth"></div>
			 <div class="col-sm-2"><input type = "text" pattern="20[0-9]{2}" required="" id = "iniYear" name = "iniYear"></div></div>
			<div class="form-group"><label class="control-label col-sm-2" for="date">Día/Mes/Año Final:</label> <div class="col-sm-2"><input type = "text" pattern="0[1-9]|1[0-9]|2[0-9]|3[01]" required="" id = "endDay" name = "endDay"></div>
			 <div class="col-sm-2"><input type = "text" pattern="0[1-9]|1[012]" required="" id = "endMonth" name = "endMonth"></div>
		     <div class="col-sm-2"><input type = "text" pattern="20[0-9]{2}" required="" id = "endYear" name = "endYear"></div></div>
			
			
		<div class="form-group">
			<label class="control-label col-sm-2">Descripción</label>
			<div class="col-sm-4">
				<textarea class="form-control" rows="5" name = "descripcion" id = "descripcion" placeholder="Escriba aquí la descripción"></textarea> </br>
			</div>
		</div>
		<div class="form-group">
      		<div class="col-sm-offset-2 col-sm-2">
        		<button type="submit" class="btn btn-default" id = "agregar" name = "agregar">Agregar</button>
			 	<button class="btn btn-default" type="button" id = "Back"
			 <%
			 	if(type == Types.ORGANIZER_COORDINATOR_TYPE){
			 %>
			 onclick = "location.href='/coordinator/interface'"
			 <%
			 	}
			 	else if(type == Types.ORGANIZER_BOSS_TYPE){
			 %>
			 onclick = "location.href='/organizer/getBossInterface'"
			 <%
			 	}
			 %>
			 >Atrás</button>
			 </div>
		</div>
		</form>
	</div>
	</body>
</html>
