<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.zs.models.ItemLog" %>
<%@ page import="com.zs.utils.Utils" %>
<%@ page import="com.zs.utils.Types" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	List<ItemLog> logsList = (List<ItemLog>) request.getAttribute("logs");
	String totalLog = "";
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Eventus 2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<li><a href="#">Inicio</a></li>
            <li><a onclick = "location.href='/inventory/interface'">Mi Panel</a></li>
            <li><a href="#">Acerca</a></li>
            <li><a href="#">Contacto</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
              <ul class="dropdown-menu">
				<li class="dropdown-header">Cuenta</li>              
                <li><a href="#">Cambiar contraseña</a></li>
                <li><a href="#">Cambiar e-mail</a></li>
                <li><a href="#">Cambiar nombre</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Avanzadas</li>
                <li><a href="#">Configuración</a></li>
                <li><a href="#">Ayuda</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="jumbotron">
	  <div class="container">
	   <h2>Historial del Inventario</h2>
	   <p> En esta sección puede revisar el historial del inventario desde sus inicios.</p>
	  </div>
	</div>
	<div class="container">
	<h2>Historial</h2>
	<button class="btn btn-default" type="button"  id = "Back" onclick = "location.href='/inventory/interface'"	>Back</button> </br><br>
	<div class="table-responsive">
		 <table class="table table-striped">
			<tr>
				<th>Fecha</th>
				<th>Elemento</th>
				<th>Modificación</th>
			</tr>
	<%
		for(ItemLog log : logsList){
			totalLog = "<td>";			
			totalLog += log.getItemName();
			totalLog += "</td>";
			switch(log.getType()){
				case Types.ITEMLOG_CREATE_TYPE:
					totalLog += "<td> fue creado </td>";
					break;
				case Types.ITEMLOG_DELETE_TYPE:
					totalLog += "<td> fue eliminado </td>";
					break;
				case Types.ITEMLOG_UPDATE_POSITIVE_TYPE:
					totalLog += "<td> aumento su cantidad en ";
					totalLog += Integer.toString(log.getExtraVal());
					totalLog += " unidades </td>";
					break;
				case Types.ITEMLOG_UPDATE_NEGATIVE_TYPE:
					totalLog += "<td> disminuyó su cantidad en ";
					totalLog += Integer.toString(log.getExtraVal());
					totalLog += " unidades </td>";
					break;
			}
	%>
		<tr>
		<td><%=Utils.getDate(log.getDate(),Types.DATE_NORMAL)%></td> <%=totalLog%>
		</tr>
	<%
		}
	%>	
	</table>
	</div>
	<button class="btn btn-default" type="button"  id = "Back" onclick = "location.href='/inventory/interface'"	>Back</button>
	</div>
	<footer><p>© 2017 ZealotSoft</p></footer>
	</body>
</html>